function checkAnswer(ticks, crosses, correct) {

  // check each id with a tick, to see if they are ALL visible
  var isCorrect = ticks.every(function(id) {
    return $(id).is(':visible')
  });

  function hideTicks(){
    for (var i = 0; i < ticks.length; i++){
        $(ticks[i]).hide();
    }
  }
  function hideCrosses(){
    for (var j = 0; j < crosses.length; j++){
        $(crosses[j]).hide();
    }
  }

  // if they were all visible, then hide the ticks and crosses and show the correct box
  if(isCorrect) {
    hideTicks();
    hideCrosses();
    $(correct).show();
    
  }
   if($(correct).is(':visible')){
   hideTicks();
   hideCrosses();
  }  
}


function checkAns(num) {
    checkAnswer(['#div1', '#div2'], ['#div4', '#div5'], '#div3');
  };

function checkAns2() {
    checkAnswer(['#div6', '#div7', '#div9', '#div10'], ['#div11', '#div12'], '#div8');
};

function checkAns3() {
    checkAnswer(['#div13', '#div14', '#div15', '#div16', '#div17'], ['#div19', '#div20'], '#div18');
};

function checkAns4() {
    checkAnswer(['#div1', '#div2', '#div21', '#div22', '#div23', '#div24'], ['#div4', '#div5'], '#div3');
};

function checkAns5() {
    checkAnswer(['#div13', '#div14', '#div15', '#div16', '#div17', '#div25', '#div26'], ['#div19'], '#div18');
};

function checkAns6() {
    checkAnswer(['#div1', '#div2'], ['#div5', '#div4'], '#div3');
};

function checkAns7() {
    checkAnswer(['#div26'], ['#div19', '#div20'], '#div18');
};

function checkAns8() {
    checkAnswer(['#div1', '#div2', '#div21', '#div22'], ['#div4', '#div5'], '#div3');
};

function checkAns9() {
    checkAnswer(['#div6', '#div7'], ['#div11', '#div12'], '#div8');
};

function checkAns10() {
    checkAnswer(['#div13'], ['#div19', '#div20'], '#div18');
};

function checkAns11() {
    checkAnswer(['#div13', '#div14', '#div15'], ['#div19', '#div20'], '#div18');
};


//var item1 = document.getElementById('questions');
var totalQuestions = $('.questions').size();
var currentQuestion = 0;
$questions = $('.questions');
$questions.hide();
$($questions.get(currentQuestion)).fadeIn();
$('#next').click(function(){
  $($questions.get(currentQuestion)).fadeOut(function(){
    currentQuestion = currentQuestion + 1;
    $(".correct, .right, .wrong, .label").hide();
    if(currentQuestion == totalQuestions){
      $("#dialog").dialog();
      $('#next').hide();
    } else {
      $($questions.get(currentQuestion)).fadeIn();
    }
  });
});


$('#hint').click(function(){
    $(this).hide();
    $("#para").hide();
    $("#vennCanvas1").show();
    $("#vennCanvas2").show();
    $(".label").show();
    $("#para1").show();
    $("#para2").show();
    $("#para3").show();
    $("#para4").show();
    clicky.goal('Clicked Ht');
});

$('#hint2').click(function(){
    $(this).hide();
    $("#para5").hide();
    $("#vennCanvas4").show();
    $("#vennCanvas5").show();
    $(".label").show();
    $("#para6").show();
    $("#para7").show();
    $("#para8").show();
    $("#para9").show();
    clicky.goal('Clicked Ht2');
});

$('#hint3').click(function(){
    $(this).hide();
    $("#para10").hide();
    $("#vennCanvas7").show();
    $("#vennCanvas8").show();
    $(".label").show();
    $("#para11").show();
    $("#para12").show();
    $("#para13").show();
    $("#para14").show();
    clicky.goal('Clicked Ht3');
});

$('#hint4').click(function(){
    $(this).hide();
    $("#para15").hide();
    $("#vennCanvas10").show();
    $("#vennCanvas11").show();
    $(".label").show();
    $("#para16").show();
    $("#para17").show();
    $("#para18").show();
    $("#para19").show();
    clicky.goal('Clicked Ht4');
});

$('#hint5').click(function(){
    $(this).hide();
    $("#para20").hide();
    $("#vennCanvas13").show();
    $("#vennCanvas14").show();
    $(".label").show();
    $("#para21").show();
    $("#para22").show();
    $("#para23").show();
    $("#para24").show();
    clicky.goal('Clicked Ht5');
});


//canvas1
   var toggleColor = (function(id){
        $("#div4").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong region');
    });
   var toggleColor2 = (function(id2){
        $("#div1").show();
        $(".wrong").hide();       
});
   var toggleColor3 = (function(id3){
        $("#div2").show();
        $(".wrong").hide();   
});
   var toggleColor4 = (function(id4){
        $("#div5").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong region');  
});
   var toggleColor18 = (function(id18){
        $("#div21").show();
        $(".wrong").hide();   
});
   var toggleColor19 = (function(id19){
        $("#div22").show();
        $(".wrong").hide();   
});
   var toggleColor20 = (function(id20){
        $("#div23").show();
        $(".wrong").hide();  
});
   var toggleColor21 = (function(id21){
        $("#div24").show();
        $(".wrong").hide();   
});


//canvas 2
   var toggleColor5 = (function(id5){
        $("#div6").show(); 
        $(".wrong").hide(); 
});
   var toggleColor6 = (function(id6){
        $("#div7").show();
        $(".wrong").hide();  
});
   var toggleColor7 = (function(id7){
        $("#div11").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong region');
});

   var toggleColor8 = (function(id8){
        $("#div12").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong region');   
});
   var toggleColor9 = (function(id9){
        $("#div9").show(); 
        $(".wrong").hide(); 
});
      var toggleColor10 = (function(id10){
        $("#div10").show();
        $(".wrong").hide();  
});


//canvas 3
   var toggleColor11 = (function(id11){
        $("#div13").show(); 
        $(".wrong").hide(); 
});
   var toggleColor12 = (function(id12){
        $("#div14").show(); 
        $(".wrong").hide(); 
});
   var toggleColor13 = (function(id13){
        $("#div15").show(); 
        $(".wrong").hide(); 
});
   var toggleColor14 = (function(id14){
        $("#div19").show().delay(2500).hide(0); 
        clicky.goal('Clicked wrong region'); 
});
   var toggleColor15 = (function(id15){
        $("#div20").show().delay(2500).hide(0);
        clicky.goal('Clicked wrong region');  
});
   var toggleColor16 = (function(id16){
        $("#div16").show(); 
        $(".wrong").hide(); 
});
      var toggleColor17 = (function(id17){
        $("#div17").show(); 
        $(".wrong").hide(); 
});
      var toggleColor22 = (function(id22){
        $("#div25").show();
        $(".wrong").hide();   
});
      var toggleColor23 = (function(id23){
        $("#div26").show();
        $(".wrong").hide();   
});


//------------------------------xxxxxxxxxxxxx------------------


function commonFunc(thePath,d){
  d3.event.stopPropagation();
  d3.select(thePath).style('fill', d.color);
}


var data = [
  {
    toggleFunc: function(d){
       toggleColor(1);
       checkAns(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor5(1);
        checkAns2(1);
    },
    color: 'magenta'
  }, {
    toggleFunc: function(d){
        toggleColor11(1);
        checkAns3(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
        toggleColor2(2);
        checkAns4(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
        toggleColor7(3);
        checkAns2(9);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor11(2);
        checkAns5(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
        toggleColor(5);
        checkAns6(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor7(5);
        checkAns2(17);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor14(4);
        checkAns7(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor2(4);
        checkAns8(1);
    },
    color: 'magenta'
  },{
    toggleFunc: function(d){
        toggleColor7(7);
        checkAns9(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor14(8);
        checkAns10(1);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor(10);
        checkAns4(9);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor7(10);
        checkAns2(25);
    },
    color: 'white'
  },{
    toggleFunc: function(d){
        toggleColor14(11);
        checkAns11(1);
    },
    color: 'white'
  }
];

var data2 = [
  {
    toggleFunc2: function(d){
       toggleColor2(1);
       checkAns(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor6(1);
       checkAns2(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
       toggleColor12(1);
       checkAn3(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
        toggleColor(4);
        checkAns4(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor8(3);
        checkAns2(10);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor14(3);
        checkAns5(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor4(4);
        checkAns6(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor8(5);
        checkAns2(18);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor15(2);
        checkAns7(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor3(4);
        checkAns8(2);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
        toggleColor8(7);
        checkAns9(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor15(5);
        checkAns10(2);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor2(5);
        checkAns4(10);
    },
    color: 'magenta'
  },{
    toggleFunc2: function(d){
        toggleColor8(10);
        checkAns2(26);
    },
    color: 'white'
  },{
    toggleFunc2: function(d){
        toggleColor15(9);
        checkAns11(2);
    },
    color: 'white'
  }
];

var data3=[ 
    {
    toggleFunc3: function(d){
       toggleColor3(1);
       checkAns(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
       toggleColor7(1);
       checkAns2(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
       toggleColor13(1);
       checkAns3(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor4(5);
        checkAns4(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
        toggleColor9(2);
        checkAns2(11);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor12(2);
        checkAns5(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor(6);
        checkAns6(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
        toggleColor9(3);
        checkAns2(19);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor14(5);
        checkAns7(3);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
        toggleColor18(2);
        checkAns8(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor5(4);
        checkAns9(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor11(3);
        checkAns10(3);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor3(5);
        checkAns4(11);
    },
    color: 'magenta'
  },{
    toggleFunc3: function(d){
        toggleColor7(11);
        checkAns2(27);
    },
    color: 'white'
  },{
    toggleFunc3: function(d){
        toggleColor14(12);
        checkAns11(3);
    },
    color: 'white'
  }
  ];

var data4=[
{
    toggleFunc4: function(d){
       toggleColor4(1);
       checkAns(4);
    },
    color: 'white'
  },{
    toggleFunc4: function(d){
       toggleColor8(1);
       checkAns2(4);
    },
    color: 'white'
    },{
    toggleFunc4: function(d){
       toggleColor14(1);
       checkAns3(4);
    },
    color: 'white'
    },{
    toggleFunc4: function(d){
        toggleColor3(2);
        checkAns4(4);
    },
    color: 'magenta'
  },{
    toggleFunc4: function(d){
        toggleColor5(2);
        checkAns2(12);
    },
    color: 'magenta'
  },{
    toggleFunc4: function(d){
        toggleColor13(2);
        checkAns5(4);
    },
    color: 'magenta'
  },{
    toggleFunc4: function(d){
        toggleColor4(6);
        checkAns6(4);
    },
    color: 'white'
  },{
    toggleFunc4: function(d){
        toggleColor5(3);
        checkAns2(20);
    },
    color: 'magenta'
  },{
    toggleFunc4: function(d){
        toggleColor15(3);
        checkAns7(4);
    },
    color: 'white'
  },{
    toggleFunc4: function(d){
        toggleColor19(2);
        checkAns8(4);
    },
    color: 'magenta'
  },{
    toggleFunc4: function(d){
        toggleColor8(8);
        checkAns9(4);
    },
    color: 'white'
  },{
    toggleFunc4: function(d){
        toggleColor15(6);
        checkAns10(4);
    },
    color: 'white'
  },{
    toggleFunc4: function(d){
        toggleColor18(3);
        checkAns4(12);
    },
    color: 'magenta'
  },{
    toggleFunc4: function(d){
        toggleColor8(11);
        checkAns2(28);
    },
    color: 'white'
  },{
    toggleFunc4: function(d){
        toggleColor15(10);
        checkAns11(4);
    },
    color: 'white'
  } 
  ];

var data5=[ 
{
    toggleFunc5: function(d){
       toggleColor(2);
       checkAns(5);
    },
    color: 'white'
  },{
    toggleFunc5: function(d){
       toggleColor7(2);
       checkAns2(5);
    },
    color: 'white'
    },{
    toggleFunc5: function(d){
       toggleColor15(1);
       checkAns3(5);
    },
    color: 'white'
    },{
    toggleFunc5: function(d){
        toggleColor18(1);
        checkAns4(5);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor6(2);
        checkAns2(13);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor16(2);
        checkAns5(5);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor4(7);
        checkAns6(5);
    },
    color: 'white'
  },{
    toggleFunc5: function(d){
        toggleColor6(3);
        checkAns2(21);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor15(4);
        checkAns7(5);
    },
    color: 'white'
  },{
    toggleFunc5: function(d){
        toggleColor(8);
        checkAns8(5);
    },
    color: 'white'
  },{
    toggleFunc5: function(d){
        toggleColor6(4);
        checkAns9(5);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor15(7);
        checkAns10(5);
    },
    color: 'white'
  },{
    toggleFunc5: function(d){
        toggleColor19(3);
        checkAns4(13);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor5(5);
        checkAns2(29);
    },
    color: 'magenta'
  },{
    toggleFunc5: function(d){
        toggleColor11(4);
        checkAns11(5);
    },
    color: 'magenta'
  }
  ];

var data6=[ 
{
    toggleFunc6: function(d){
       toggleColor4(2);
       checkAns(6);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
       toggleColor9(1);
       checkAns2(6);
    },
    color: 'magenta'
    },{
    toggleFunc6: function(d){
       toggleColor16(1);
       checkAns3(6);
    },
    color: 'magenta'
    },{
    toggleFunc6: function(d){
        toggleColor19(1);
        checkAns4(6);
    },
    color: 'magenta'
  },{
    toggleFunc6: function(d){
        toggleColor7(4);
        checkAns2(14);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor17(2);
        checkAns5(6);
    },
    color: 'magenta'
  },{
    toggleFunc6: function(d){
        toggleColor(7);
        checkAns6(6);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor7(6);
        checkAns2(22);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor14(6);
        checkAns7(6);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor4(8);
        checkAns8(6);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor7(8);
        checkAns9(6);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor14(9);
        checkAns10(6);
    },
    color: 'white'
  },{
    toggleFunc6: function(d){
        toggleColor20(2);
        checkAns4(14);
    },
    color: 'magenta'
  },{
    toggleFunc6: function(d){
        toggleColor6(5);
        checkAns2(30);
    },
    color: 'magenta'
  },{
    toggleFunc6: function(d){
        toggleColor12(3);
        checkAns11(6);
    },
    color: 'magenta'
  }
  ];

var data7=[ 
{
    toggleFunc7: function(d){
       toggleColor(3);
       checkAns2(7);
    },
    color: 'white'
  },{
    toggleFunc7: function(d){
       toggleColor8(2);
       checkAns2(8);
    },
    color: 'white'
    },{
    toggleFunc7: function(d){
       toggleColor14(2);
       checkAns3(8);
    },
    color: 'white'
    },{
    toggleFunc7: function(d){
        toggleColor20(1);
        checkAns4(7);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor10(2);
        checkAns2(15);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor22(1);
        checkAns5(7);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor2(3);
        checkAns6(7);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor10(3);
        checkAns2(23);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor23(2);
        checkAns7(7);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor4(9);
        checkAns8(7);
    },
    color: 'white'
  },{
    toggleFunc7: function(d){
        toggleColor7(9);
        checkAns9(7);
    },
    color: 'white'
  },{
    toggleFunc7: function(d){
        toggleColor14(10);
        checkAns10(7);
    },
    color: 'white'
  },{
    toggleFunc7: function(d){
        toggleColor21(2);
        checkAns4(15);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor9(4);
        checkAns2(31);
    },
    color: 'magenta'
  },{
    toggleFunc7: function(d){
        toggleColor13(3);
        checkAns11(7);
    },
    color: 'magenta'
  }
  ];


//Render Venn Diagrams
var wholeV = d3.selectAll('.v1, .v2, .v3');
var wholeVCanvas = wholeV.append("svg").attr("width", 200).attr("height", 190);

wholeVCanvas.data(data)
  .append("svg:path")
  .attr("d", "M100 15 A 54 50, 0, 1, 0, 40 119 A 70 70, 0, 0, 1, 73 60 A 55 55, 0, 0, 1, 100 15")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc(d);
  }); 

wholeVCanvas.data(data2)
  .append("svg:path")
  .attr("d", "M100 15 A 55 55, 0, 0, 0, 73 61 A 55 55, 0, 0, 1, 127 61 A 64 64, 0, 0, 0, 100 15")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc2(d);
  }); 

wholeVCanvas.data(data3)
  .append("svg:path")
  .attr("d", "M73 61 A 55 55, 0, 0, 1, 127 61 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 0, 1, 73 61")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc3(d);
  }); 

wholeVCanvas.data(data4)
  .append("svg:path")
  .attr("d", "M73 61 A 60 60, 0, 0, 0, 100 115 A 60 60, 0, 0, 1, 40 119 A 60 60, 0, 0, 1, 73 61")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc4(d);
  }); 

wholeVCanvas.data(data5)
  .append("svg:path")
  .attr("d", "M127 61 A 60 60, 0, 0, 1, 100 115 A 60 60, 0, 0, 0, 160 118 A 60 60, 0, 0, 0, 127 61")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc5(d);
  }); 

wholeVCanvas.data(data6)
  .append("svg:path")
  .attr("d", "M127 61 A 60 60, 0, 0, 1, 160 118 A 10 10, 0, 0, 0, 100 15 A 60 60, 0, 0, 1, 127 61")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc6(d);
  }); 

wholeVCanvas.data(data7)
  .append("svg:path")
  .attr("d", "M100 115 A 60 60, 0, 0, 1, 40 119 A 55 55, 0, 0, 0 160 119 A 60 60, 0, 0, 1, 100 115")
  .style("stroke","black")
  .style("fill", "white")
  .style("stroke-width", 1)
  .on('click', function(d){
    commonFunc(this, d);
    d.toggleFunc7(d);
  }); 



//--------------------------------xxxxxxx-----------------------

    
$("#vennCanvas1").click(function(){
    toggleColor4(3);
    checkAns(8);
    clicky.goal('Clicked wrong region');
});
    
$("#vennCanvas2").click(function(){
    $("#vennCanvas2").css('background', 'magenta');
    toggleColor10(1);
    checkAns2(8);
});

$("#vennCanvas3").click(function(){
    $("#vennCanvas3").css('background', 'magenta');
    toggleColor17(1);
    checkAns3(8);
});

$("#vennCanvas4").click(function(){
    $("#vennCanvas4").css('background', 'magenta');
    toggleColor21(1);
    checkAns4(8);
});

$("#vennCanvas5").click(function(){
    toggleColor8(4);
    checkAns2(16);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas6").click(function(){
    $("#vennCanvas6").css('background', 'magenta');
    toggleColor23(1);
    checkAns5(8);
});

$("#vennCanvas7").click(function(){
    $("#vennCanvas7").css('background', 'magenta');
    toggleColor3(3);
    checkAns6(8);
});

$("#vennCanvas8").click(function(){
    toggleColor8(6);
    checkAns2(24);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas9").click(function(){
    toggleColor14(7);
    checkAns7(8);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas10").click(function(){
    toggleColor(9);
    checkAns8(8);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas11").click(function(){
    toggleColor8(9);
    checkAns9(8);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas12").click(function(){
    toggleColor15(8);
    checkAns10(8);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas13").click(function(){
    toggleColor4(10);
    checkAns4(16);
    clicky.goal('Clicked wrong region');
});

$("#vennCanvas14").click(function(){
    $("#vennCanvas14").css('background', 'magenta');
    toggleColor10(4);
    checkAns2(32);
});

$("#vennCanvas15").click(function(){
    toggleColor14(13);
    checkAns11(8);
});
